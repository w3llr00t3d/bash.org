        #963025 +(293)- [X]

        <mogh> I landed an in person interview
        <sergey> where at?
        <alexander> Hydraulic Press Inc.
        <sergey> You've gotta crush the interview to work there
        <sergey> very firm handshake is a plus as well
        <kurn> dunno if I'd want to work there, I hear it's a
        high-pressure environment
        <kurn> and opportunities for advancement are pretty flat     
            
        <alexander> Getting the wrong manager there can be
        soul-crushing.     
        <helena> but getting the right one can be powerfully
        uplifting          
        <mogh> I think I could squeeze out a living

