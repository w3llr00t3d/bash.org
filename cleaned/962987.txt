        #962987 +(-76)- [X]

        <randymarsh9> what's the proper way to administer a remote
        linux server?
        <aeyxa> ssh
        <phogg> randymarsh9: Hire me to use ssh.
        <randymarsh9> phogg: do you take bitcoin
        <phogg> randymarsh9: no, I take payment only in Linux support.
        That YOU provide, later.
        <phogg> to other people
        <randymarsh9> what is ssh file transfer protocol claled
        <aeyxa> sftp
        <SporkWitch> scp
        <phogg> sftp > scp, but sure you can use scp too
        <randymarsh9> ok i want to use scp but whenever i try to edit/
        move file i get access denied
        <randymarsh9> only root has access to modify those files
        <SporkWitch> you've answered your own question then
        <randymarsh9> i can't login as root though
        <randymarsh9> there's gotta be a simple way to do this without
        changing folder permissions everytime
        <randymarsh9> i know how from ssh but want to do the same from
        scp
        <aeyxa> There is, you log in as root
        <randymarsh9> no, i turned that shit off because some stupid
        security blog told me it's the right thing to do
        <randymarsh9> you are basically telling me the hard way to do
        it
        <randymarsh9> i already know how to do it the hard way i was
        looking for something easier through scp
        <randymarsh9> telling me to login as root is hardly an answer
        <aeyxa> randymarsh9: Log in as your user, then sudo up to root
        <randymarsh9> aeyxa: how do you sudo up in scp
        <aeyxa> randymarsh9: What files are you moving, where are you
        moving them from, what have you tried.
        <randymarsh9> aeyxa: want to edit files in /etc/
        <aeyxa> randymarsh9: Do you have permission to use the root
        account?
        <randymarsh9> yes, but the login through scp with root is
        turned off
        <aeyxa> randymarsh9: If you only want to edit files, I don't
        know why you're asking about scp and sftp.
        <randymarsh9> aeyxa: because if i do it through scp i'm able
        to use the gui

